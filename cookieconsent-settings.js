window.addEventListener('load', function () {
    // obtain cookieconsent plugin
    var cookieconsent = initCookieConsent();

    // run plugin with config object
    cookieconsent.run({
        autorun: true,
        revision: 1,
        current_lang: document.documentElement.getAttribute('lang'),
        theme_css: '/wp-content/themes/wp-bootstrap-starter-child-master/assets/css/cookieconsent.css',
        autoclear_cookies: true,
        page_scripts: true,

        onAccept: function (cookie) {
            
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onAccept)!");
        },

        onChange: function (cookie, changed_preferences) {
            dataLayer.push({'event': 'update', 'cookie': JSON.stringify(cookie)});
            console.log("PUSH UPDATE (onChange)!");
        },

        languages: {
            cs: {
                consent_modal: {
                    title: 'Nastavení používání cookies',
                    description: 'Na těchto webových stránkách používám cookies. Kromě nutných cookies pro fungování stránek používám i analytické cookies, díky kterým mohu sledovat návštěvnost mého webu.<br><br> Svůj souhlas můžete kdykoliv odvolat, nebo nastavit souhlas jen s některými z nich kliknutím na "upravit nastavení", či <a id="nastavitnutne" type="button" class="cc-link">přijmout jen nezbytné</a>.<br><br><a href="https://www.libor-matejka.cz/cookies/" target="_new">Více o používání cookies</a>.',
                    primary_btn: {
                        text: 'Přijmout všechny',
                        role: 'accept_all'              // 'accept_selected' or 'accept_all'
                    },
                    secondary_btn: {
                        text: 'Upravit nastavení',
                        role: 'settings'                // 'settings' or 'accept_necessary'
                    }
               },
                settings_modal: {
                    title: 'Nastavení cookies',
                    save_settings_btn: 'Uložit nastavení',
                    accept_all_btn: 'Přijmout vše',
                    cookie_table_headers: [
                        {col1: 'Jméno'},
                        {col2: 'Doména'},
                        {col3: 'Expirace'},
                        {col4: 'Popis'},
                    ],
                    blocks: [
                        {
                            title: 'Použité cookie',
                            description: 'Na webu používám cookies. Můžete si vybrat, které cookies povolíte.'
                        }, {
                            title: 'Nutné cookies',
                            description: 'Tyto cookies jsou opravdu potřebné pro základní funkčnost webu. Bez nich by web nemusel fungovat správně.',
                            toggle: {
                                value: 'necessary',
                                enabled: true,
                                readonly: true
                            },
							cookie_table: [
                                {
                                    col1: 'cc_cookie',
                                    col2: 'libor-matejka.cz',
                                    col3: '6 měsíců',
                                    col4: 'Ukládání informací o (ne)udělení souhlasu s cookies',
                                }
                            ]
                        }, {
                            title: 'Analytické cookies',
                            description: 'Tyto cookies sbírají informace o průchodu návštěvníka našimi stránkami . Všechny tyto cookies jsou anonymní a nemohu návštěvníka nijak identifikovat. Tyto cookies nejsou nutné pro fungování webu jako takového, ale pomáhají nám tyto stránky kontinuálně zlepšovat.',
                            toggle: {
                                value: 'analytics',
                                enabled: false,
                                readonly: false
                            },
							cookie_table: [
                                {
                                    col1: '^_ga',
                                    col2: 'google.com',
                                    col3: '2 roky',
                                    col4: 'Cookie slouží k měření návštěvnosti na webu',
                                    is_regex: true
                                },
                                {
                                    col1: '_gid',
                                    col2: 'google.com',
                                    col3: '1 den',
									col4: 'Cookie slouží k měření návštěvnosti na webu',
                                }
                            ]
                        },                      
                        {
                            title: 'Více informací',
                            description: 'Více informací o zpracování osobních údajů na mých webových stránkách najdete <a class="cc-link" target="_blank" href="https://www.libor-matejka.cz/cookies/">zde</a>.',
                        }
                    ]
                }
            }
        }
    });

    var nastavitNutne = document.getElementById("nastavitnutne");

    if( nastavitNutne ){

        nastavitNutne.onclick = function () {
            cookieconsent.hide();
            cookieconsent.accept([]);
        };

    }

});